  
  Drupal.behaviors.trackResultView = function (context) {
  $('.node-results dt.title a:not(.trackResultView-processed)', context).addClass('trackResultView-processed').click( function(e){
      var keys = $("form input[name='keys']").attr('value');
      $.get("/relevance_feedback/js/", { url: this.href, keys: keys, type: "rel"});
  		
    });
  };
