  
  Drupal.behaviors.relevanceImage = function (context) {
  $('.node-results img.relevance:not(.relevanceImage-processed)', context).
    addClass('relevanceImage-processed').
    css('cursor', 'pointer'). 
    click( function(e){
      var nid = $(this).parent().get(0).id.substr(19);
      var type = this.src.match(/up/) ? true : false;
      if (relevance.getStatus(nid) === type)
        relevance.setStatus(nid, null);
      else
        relevance.setStatus(nid, type);

    });
    
  };

  
  Drupal.relevanceFeedback  = function () {
    this.status = {};
  }
  
  Drupal.relevanceFeedback.prototype.setStatus = function(nid, on) {
    this.status[nid] = on;
    
    var keys = $("form input[name='keys']").attr('value');
    var type = on ? 'rel' : (on == null ? 'undef' : 'nonrel');
    
    $.get("/relevance_feedback/js/", { nid: nid, keys: keys, type: type}, function(data){
      if ( data == 1 )
        location.reload(); 
      }
    );

    //change image accordingly
    $("span[id='relevance_feedback_" + nid + "'] img.up").each( function(e) {
	    var base = this.src.replace(/_on/, "").replace(/up\.gif/, "");
	    this.src = on ? base + "up_on.gif" : base + "up.gif";
	  });
    $("span[id='relevance_feedback_" + nid + "'] img.down").each( function(e) {
	    var base = this.src.replace(/_on/, "").replace(/down\.gif/, "");
	    this.src = on === false ? base + "down_on.gif" : base + "down.gif";
	  });
  }
  
  Drupal.relevanceFeedback.prototype.getStatus = function(nid) {
    return this.status[nid];
  }
  
  
  $(document).ready(function() {
  	relevance = new Drupal.relevanceFeedback;

    //parse the inital status
    $("span.relevance_feedback").each( function(e) {
      if ($(this).attr('rf_status') != null) {
        var nid = this.id.substr(19);
        relevance.setStatus(nid, $(this).attr('rf_status') == "1" ? true : false);
      }
	  });
  });
  