<?php
// $Id: relevance_feedback.admin.inc,v 1.1 2008/09/23 16:18:52 fago Exp $

/**
 * @file
 * Admin page callbacks for the relevance_feedback module.
 */


/**
 * Form builder; Configure relevance_feedback.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function relevance_feedback_settings() {

  $form['relevance_feedback_type'] = array(
    '#type' => 'radios',
    '#title' => t('How to get feedback'),
    '#default_value' => variable_get('relevance_feedback_type', 'explicit'),
    '#options' => array(
      'explicit' => t('Users can mark search results as (non)relevant.'),
      'implicit' => t('Treat results viewed by users as relevant.'),
     ),
  );
  $form['relevance_feedback_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Refine search when feedback for how many results is available'),
    '#default_value' => variable_get('relevance_feedback_count', 5),
    '#description' => t('After the user has provided positive feedback for  e.g. 5 results, his search will be refined.'),
  );
  $form['relevance_feedback_algo'] = array(
    '#type' => 'radios',
    '#title' => t('Algorithm'),
    '#default_value' => variable_get('relevance_feedback_algo', 'rocchio'),
    '#options' => array(
      'rocchio' => t('Rocchio algorithm'),
     ),
  );
  $form['relevance_feedback_treshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Threshold for adding new terms to the query'),
    '#default_value' => variable_get('relevance_feedback_treshold', 0.10),
  );
  $form['relevance_feedback_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug mode'),
    '#default_value' => variable_get('relevance_feedback_debug', 0),
  );
  $form['rocchio'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rocchio algorithm'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rocchio']['relevance_feedback_rocchio_alpha'] = array(
    '#type' => 'textfield',
    '#title' => t('Alpha value'),
    '#default_value' => variable_get('relevance_feedback_rocchio_alpha', 1),
  );
  $form['rocchio']['relevance_feedback_rocchio_beta'] = array(
    '#type' => 'textfield',
    '#title' => t('Beta value'),
    '#default_value' => variable_get('relevance_feedback_rocchio_beta', 0.75),
  );
  $form['rocchio']['relevance_feedback_rocchio_gamma'] = array(
    '#type' => 'textfield',
    '#title' => t('Gamma value'),
    '#default_value' => variable_get('relevance_feedback_rocchio_gamma', 0.15),
  );
  return system_settings_form($form);
}